'use strict';

var app = angular.module('iptablesWebApi', []);

app.controller('iptablesCtrl', function($scope, $http, $interval) {

  $scope.addrule = {
    address:'',
    port:'',
    protocol:''
  }

  function update() {
      $http.get("/api/rules", {
      }).then(function (response) {
          if (response.status == 200) {
            $scope.rules = response.data.rules;
          }
      });

      $http.get("/api/hosts", {
      }).then(function (response) {
          if (response.status == 200) {
            $scope.hosts = response.data.hosts;
          }
      });
  }

  $scope.addRule = function() {
    $http.post("/api/rules", $scope.addrule
    ).then(function (response) {
        if (response.status == 200) {
          update();
        }
    });
  }

  $scope.delRule = function(rule) {
    console.log(rule);
    $http.delete("/api/rules", {data: rule, headers: {
        'Content-type': 'application/json;charset=utf-8'
    }}
    ).then(function (response) {
        if (response.status == 200) {
          update();
        }
    });
  }

  update();
});
