#!/usr/bin/env python3

import iptc
import random
from flask import Flask, request, Response, json
from isc_dhcp_leases.iscdhcpleases import Lease, IscDhcpLeases

app = Flask(__name__)

def random_check(ports):
    new_port = str(random.randint(34000,34100))
    while new_port in ports:
        new_port = str(random.randint(34000,34100))
    return new_port

def resp(data):
    return Response(json.dumps(data),  mimetype='application/json')

def get_rules():
    table = iptc.Table(iptc.Table.NAT)
    table.refresh()
    rules = []
    for rule in table.chains[0].rules:
        address, port = rule.target.parameters['to_destination'].split(':')
        new_rule = {'dport':rule.matches[0].parameters['dport'], \
        'protocol':rule.protocol,\
        'address':address,
        'port':port}
        rules.append(new_rule)
    return resp({'rules':rules})

def add_rule(content):

    if not 'address' in content:
        return resp({'error':'no address'})
    elif  content['address'] == '':
        return resp({'error':'bad address'})
    elif not content['address'].startswith("10.160.19."):
        return resp({'error':'wrong address'})

    add_address = content['address']

    if not 'port' in content:
        return resp({'error':'no port'})
    elif  content['port'] == '':
        return resp({'error':'bad port'})
    else:
        add_port = content['port']

    if not 'protocol' in content:
        return resp({'error':'no protocol'})
    elif  content['protocol'] == '':
        return resp({'error':'bad protocol'})
    else:
        add_protocol = content['protocol']

    table = iptc.Table(iptc.Table.NAT)
    table.refresh()
    rules = []
    for rule in table.chains[0].rules:
        address, port = rule.target.parameters['to_destination'].split(':')
        protocol = rule.protocol
        if add_address == address:
            if add_port == port:
                if add_protocol == protocol:
                    return resp({'ext_port':\
                    rule.matches[0].parameters['dport']})
    ports = []
    for rule in table.chains[0].rules:
        ports.append(rule.matches[0].parameters['dport'])
    new_port = random_check(ports)

    chain = iptc.Chain(iptc.Table(iptc.Table.NAT), "PREROUTING")
    rule = iptc.Rule()
    rule.protocol = content['protocol']
    rule.dst = '10.160.18.5/255.255.255.255'
    match = rule.create_match('tcp')
    match.dport = new_port
    target = iptc.Target(rule, "DNAT")
    target.to_destination = ':'.join([add_address,add_port])
    rule.target = target
    chain.insert_rule(rule)

    return resp({'ext_port':new_port})


def delete_rule(content):

    if not 'address' in content:
        return resp({'error':'no address'})
    elif  content['address'] == '':
        return resp({'error':'bad address'})
    elif not content['address'].startswith("10.160.19."):
        return resp({'error':'wrong address'})

    add_address = content['address']

    if not 'port' in content:
        return resp({'error':'no port'})
    elif  content['port'] == '':
        return resp({'error':'bad port'})
    else:
        add_port = content['port']

    if not 'protocol' in content:
        return resp({'error':'no protocol'})
    elif  content['protocol'] == '':
        return resp({'error':'bad protocol'})
    else:
        add_protocol = content['protocol']

    table = iptc.Table(iptc.Table.NAT)
    table.refresh()
    rules = []
    for rule in table.chains[0].rules:
        address, port = rule.target.parameters['to_destination'].split(':')
        protocol = rule.protocol
        if add_address == address:
            if add_port == port:
                if add_protocol == protocol:
                    table.chains[0].delete_rule(rule)
                    return resp({'deleted':''})
    return resp({'error':'not found'})

@app.route("/rules",methods=['GET','POST','DELETE'])
def rules():
    if request.method == 'GET':
        return get_rules()
    if request.method == 'POST':
        content = request.get_json(silent=True)
        return add_rule(content)
    if request.method == 'DELETE':
        content = request.get_json(silent=True)
        return delete_rule(content)

@app.route("/hosts",methods=['GET'])
def hosts():
    leases = IscDhcpLeases('/var/lib/dhcp/dhcpd.leases')
    leases = leases.get()
    hosts = []
    for lease in leases:
        if lease.active:
            host = {'ip':lease.ip,'hostname':lease.hostname}
            hosts.append(host)
    return resp({'hosts':hosts})

if __name__ == "__main__":
    app.run(host='0.0.0.0')
